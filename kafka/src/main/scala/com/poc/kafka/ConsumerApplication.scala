package com.poc.kafka

import akka.actor.{ActorSystem, Props}
import akka.stream.ActorMaterializer
import com.poc.kafka.consumer.Consumer
import com.typesafe.config.ConfigFactory

import scala.concurrent.ExecutionContextExecutor

object ConsumerApplication extends App {
  val actorSystem = ActorSystem("kafka")
  implicit val executionContext: ExecutionContextExecutor = actorSystem.dispatcher
  implicit val actorMat: ActorMaterializer = ActorMaterializer.create(actorSystem)
  val config = ConfigFactory.load()
  val receiver = actorSystem.actorOf(Props(new Receiver))

  val consumer = new Consumer(config.getConfig("consumer"), receiver)
  consumer.start
  sys.addShutdownHook(() => actorSystem.terminate())
}


package com.poc.kafka

import akka.actor.{ActorSystem, Props}
import akka.stream.ActorMaterializer
import com.poc.kafka.domain.message.Event
import com.poc.kafka.domain.message.KafkaMessage.toKafkaMessage
import com.poc.kafka.producer.Producer
import com.typesafe.config.ConfigFactory

import scala.concurrent.ExecutionContextExecutor

object ProducerApplication extends App {
  val actorSystem = ActorSystem("kafka")
  implicit val executionContext: ExecutionContextExecutor = actorSystem.dispatcher
  implicit val actorMat: ActorMaterializer = ActorMaterializer.create(actorSystem)
  val config = ConfigFactory.load()
  val producer = new Producer(config.getConfig("producer"))
  (1 to 100).foreach { i =>
    producer.produce(Event("1", s"Run $i").kafkaMessage)
  }
  actorSystem.terminate()
}

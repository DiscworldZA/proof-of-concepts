package com.poc.kafka.producer

import akka.Done
import akka.kafka.ProducerSettings
import akka.kafka.scaladsl.Producer
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Flow, Source}
import com.poc.kafka.domain.message.KafkaMessage
import com.typesafe.config.Config
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.StringSerializer

import scala.concurrent.{ExecutionContext, Future}

class Producer(val config: Config)(implicit executionContext: ExecutionContext, actorMat: ActorMaterializer) {

  private val topic = config.getString("topic")
  private val bootstrapServers = config.getString("bootstrap-servers")
  private val producerSettings: ProducerSettings[String, String] = ProducerSettings[String, String](config, new StringSerializer, new StringSerializer).withBootstrapServers(bootstrapServers)
  private val producerFlow = Producer.plainSink[String, String](producerSettings)

  private def producerSource(element: KafkaMessage) = Source.single(element)

  private val producerRecordFlow = Flow.fromFunction {
    message: KafkaMessage => new ProducerRecord[String, String](topic, message.id, message.data)
  }

  def produceAsync(msg: KafkaMessage): Future[Done] = {
    producerSource(msg).via(producerRecordFlow).runWith(producerFlow)
  }

  def produce(msg: KafkaMessage): Done = {
    producerSource(msg).via(producerRecordFlow).to(producerFlow).run()
    Done
  }

}

package com.poc.kafka.consumer

import java.util.concurrent.TimeUnit

import akka.Done
import akka.actor.ActorRef
import akka.kafka.scaladsl.Consumer.DrainingControl
import akka.kafka.scaladsl.{Committer, Consumer}
import akka.kafka.{CommitterSettings, ConsumerSettings, Subscriptions}
import akka.pattern.ask
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Keep, Sink}
import akka.util.Timeout
import com.poc.kafka.domain.message.KafkaMessage
import com.typesafe.config.Config
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.StringDeserializer

import scala.collection.immutable
import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

class Consumer(val config: Config, val receiver: ActorRef)(implicit executionContext: ExecutionContext, actorMat: ActorMaterializer) {

  private val timeoutCount = config.getDuration("processing-time")
  implicit private val timeout: Timeout = Timeout(FiniteDuration(timeoutCount.getSeconds, TimeUnit.SECONDS))
  private val topic = config.getString("topic")
  private val consumerSettings = ConsumerSettings[String, String](config, new StringDeserializer, new StringDeserializer)
    .withBootstrapServers(config.getString("bootstrap-servers"))
    .withGroupId("application-id")
    .withProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest")
  private val committerSettings = CommitterSettings(config.getConfig("committer"))

  private val consumerSource = Consumer.committableSource(consumerSettings, Subscriptions.topics(topic)).mapAsync(10) { record =>
    (receiver ? KafkaMessage(record.record.key(), record.record.value())).map {
      case Done => Option(record.committableOffset)
      case _ => None
    }.map(_.getOrElse(throw new Exception(s"Unable to commit $record")))
  }.via(Committer.flow(committerSettings))
    .toMat(Sink.seq)(Keep.both)
    .mapMaterializedValue(DrainingControl.apply)

  def start: DrainingControl[immutable.Seq[Done]] = {
    consumerSource.run
  }
}

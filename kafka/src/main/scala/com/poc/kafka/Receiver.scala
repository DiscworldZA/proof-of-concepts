package com.poc.kafka

import akka.Done
import akka.actor.{Actor, ActorLogging}
import com.poc.kafka.domain.message.{Event, KafkaMessage}

import scala.concurrent.{ExecutionContext, Future}

class Receiver extends Actor with ActorLogging {

  implicit val ex: ExecutionContext = context.dispatcher

  override def receive: Receive = {
    case record: KafkaMessage =>
      Future {
        log.info(s"Got ${record.as[Event]}")
      }
      sender() ! Done
  }
}

package com.poc.kafka.domain.message

import play.api.libs.json.{Json, OFormat}

case class Event(
  id: String,
  name: String
) extends KafkaMessageAble

object Event {
  implicit val format: OFormat[Event] = Json.format[Event]
}
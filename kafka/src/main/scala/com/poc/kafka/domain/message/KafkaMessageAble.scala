package com.poc.kafka.domain.message

trait KafkaMessageAble {
  def id: String
}

package com.poc.kafka.domain.message

import play.api.libs.json.{Format, JsError, JsSuccess, Json}

case class KafkaMessage(
  id: String,
  data: String
) {
  def as[T](implicit format: Format[T]): T = {
    Json.parse(data).validate[T] match {
      case JsSuccess(value, _) => value
      case JsError(errors) => throw new Exception(errors.toString())
    }
  }
}

object KafkaMessage {

  implicit class toKafkaMessage[T <: KafkaMessageAble](clazz: T)(implicit format: Format[T]) {
    def kafkaMessage: KafkaMessage = {
      KafkaMessage(clazz.id, format.writes(clazz).toString())
    }

  }

}

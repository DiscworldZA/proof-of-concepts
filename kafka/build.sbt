
ThisBuild / scalaVersion     := "2.12.7"
ThisBuild / version          := "0.1.0"
ThisBuild / organization     := "com.poc"
ThisBuild / organizationName := "poc"

lazy val root = (project in file("."))
  .settings(
    name := "kafka",
    libraryDependencies ++= Seq(
      "org.scalatest" %% "scalatest" % "3.0.8" % Test,
      "com.typesafe.akka" %% "akka-stream" % "2.5.23",
      "com.typesafe.akka" %% "akka-stream-kafka" % "1.0.4",
      "com.typesafe.play" %% "play-json" % "2.7.3"
    )
  )

// See https://www.scala-sbt.org/1.x/docs/Using-Sonatype.html for instructions on how to publish to Sonatype.
